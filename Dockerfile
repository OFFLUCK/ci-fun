FROM gradle:jdk17-alpine

WORKDIR /app
COPY . /app

RUN ./gradlew build
RUN ./gradlew test
