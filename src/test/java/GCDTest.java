import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class GCDTest {
    final int NUMBER_OF_STRESS_TESTS = 1;
    final int MIN = 0;
    final int MAX = 100;
    final GCD gcd = new GCD();

    @Test
    void block1() {
        assertEquals(20, gcd.gcd(100, 60));
        assertEquals(1, gcd.gcd(1, 1));
        assertEquals(1, gcd.gcd(2, 1));
        assertEquals(1, gcd.gcd(1, 2));
        System.out.println("Block 1 is OK!");
    }

    @Test
    void block2() {
        assertEquals(20, gcd.gcd(-100, 60));
        assertEquals(20, gcd.gcd(100, -60));
        assertEquals(20, gcd.gcd(-100, -60));
        assertEquals(1, gcd.gcd(-1, 1));
        assertEquals(1, gcd.gcd(1, -1));
        assertEquals(1, gcd.gcd(-1, -1));
        assertEquals(1, gcd.gcd(-2, 1));
        assertEquals(1, gcd.gcd(2, -1));
        assertEquals(1, gcd.gcd(-2, -1));
        assertEquals(1, gcd.gcd(-1, 2));
        assertEquals(1, gcd.gcd(1, -2));
        assertEquals(1, gcd.gcd(-1, -2));
        System.out.println("Block 2 is OK!");
    }

    @Test
    void block3() {
        assertEquals(60, gcd.gcd(0, 60));
        assertEquals(100, gcd.gcd(100, 0));
        assertEquals(1, gcd.gcd(0, 1));
        assertEquals(1, gcd.gcd(1, 0));
        assertEquals(1, gcd.gcd(1, 0));
        assertEquals(1, gcd.gcd(0, 1));
        assertEquals(2, gcd.gcd(0, 2));
        assertEquals(2, gcd.gcd(2, 0));
        assertEquals(2, gcd.gcd(2, 0));
        assertEquals(2, gcd.gcd(0, 2));
        assertEquals(0, gcd.gcd(0, 0));
        System.out.println("Block 3 is OK!");
    }

    @Test
    void block4() {
        assertEquals(1, gcd.gcd(5, 3));
        assertEquals(1, gcd.gcd(-5, 3));
        assertEquals(1, gcd.gcd(5, -3));
        assertEquals(1, gcd.gcd(-5, -3));
        assertEquals(1, gcd.gcd(15, 143));
        assertEquals(1, gcd.gcd(-15, 143));
        assertEquals(1, gcd.gcd(15, -143));
        assertEquals(1, gcd.gcd(-15, -143));
        System.out.println("Block 4 is OK!");
    }

    @Test
    void block5() {
        assertEquals(100, gcd.gcd(100, 100));
        assertEquals(100, gcd.gcd(-100, -100));
        assertEquals(1, gcd.gcd(1, 1));
        assertEquals(1, gcd.gcd(-1, -1));
        assertEquals(0, gcd.gcd(0, 0));
        assertEquals(0, gcd.gcd(-0, -0));
        System.out.println("Block 5 is OK!");
    }

    @Test
    void block6() {
        assertEquals(20, gcd.gcd(100, 20));
        assertEquals(20, gcd.gcd(-100, 20));
        assertEquals(20, gcd.gcd(100, -20));
        assertEquals(20, gcd.gcd(-100, -20));
        assertEquals(20, gcd.gcd(20, 100));
        assertEquals(20, gcd.gcd(-20, 100));
        assertEquals(20, gcd.gcd(20, -100));
        assertEquals(20, gcd.gcd(-20, -100));
        System.out.println("Block 6 is OK!");
    }

    @Test
    void block7() {
        assertEquals(4, gcd.gcd(12, 8));
        assertEquals(4, gcd.gcd(-12, 8));
        assertEquals(4, gcd.gcd(12, -8));
        assertEquals(4, gcd.gcd(-12, -8));
        System.out.println("Block 7 is OK!");
    }

    @Test
    void block8() {
        assertEquals(1, gcd.gcd(1, 1));
        assertEquals(1, gcd.gcd(144, 233));
        assertEquals(1, gcd.gcd(10946, 17711));
        assertEquals(1, gcd.gcd(28657, 46368));
        System.out.println("Block 9 is OK!");
    }
}
